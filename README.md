# raffle-script
A very basic, randomized raffle script in Python.

###GPLv3

### -----------------------------------
The script will read an file called *attendees.txt* - a line-separated list of names, and randomly pick a winner.

The winner's name will be removed from the list for successive draws.

Run the script as:

```Python
python raffle.py
```