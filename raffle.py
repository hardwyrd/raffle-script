#!/usr/bin/env python

# raffle.py
# a raffle script by hardwyrd
#
# GPLv3
# 
# -----------------------------------
# The script will read an file called
# 'attendees.txt' - a line-separated
# list of names, and randomly pick a
# winner.
#
# The winner's name will be removed
# from the list for successive draws.

# Run the script as:
#
# python raffle.py

# import the randome module
import random

# read the file into a list variable
with open('attendees.txt', 'rU') as f:
    itgx = f.read().split('\n')

# randomly get a winner
winner = random.choice(itgx)

# print the name of the winner 
print('Winner: %s' % winner)

# remove the winner from the list
itgx.remove(winner)

# write the new list into the file
with open('attendees.txt', 'w') as f:
    f.write('\n'.join(itgx))
